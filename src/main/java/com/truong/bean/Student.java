package com.truong.bean;

public class Student {

	private int id;
	private String name;
	private int age;
	private String className;
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(int id, String name, int age, String className) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.className = className;
	}
	public Student(String name, int age, String className) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.age = age;
		this.className = className;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	
	
	
	
}

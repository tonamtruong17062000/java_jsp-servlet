package com.truong.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.truong.bean.Student;

public class StudentDAO {
	
	public static Connection connection = ConnectionDB.createConnection();
	
	public static List<Student> getAllStudent(){
		
		ArrayList<Student> list = new ArrayList<Student>();
		
		String querySelect = "select * from student";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(querySelect);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				int id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				int age = resultSet.getInt(3);
				String className = resultSet.getString(4);
				list.add(new Student(id, name, age, className));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	public static boolean store(Student student) {
		boolean check = true;
		String queryInsert = "insert into student (name, age, class) values (?, ?, ?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(queryInsert);
			preparedStatement.setString(1, student.getName());
			preparedStatement.setInt(2, student.getAge());
			preparedStatement.setString(3, student.getClassName());
			check = preparedStatement.executeUpdate() == 1;
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;
	}
	
	public static Student getStudent(int idCurrent) {
		Student student = new Student();
		String query = "select * from student where id = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, idCurrent);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				student.setId(resultSet.getInt(1));
				student.setName(resultSet.getString(2));
				student.setAge(resultSet.getInt(3));
				student.setClassName(resultSet.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student;
	}
	
	public static boolean update(Student student) {
		boolean check = true;
		String queryInsert = "update student set name=?, age=?, class=? where id = ?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(queryInsert);
			preparedStatement.setString(1, student.getName());
			preparedStatement.setInt(2, student.getAge());
			preparedStatement.setString(3, student.getClassName());
			preparedStatement.setInt(4, student.getId());
			check = preparedStatement.executeUpdate() == 1;
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;
	}
	
	public static boolean delete(int idCurrent) {
		boolean check = true;
		String query = "delete from student where id=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, idCurrent);
			check = preparedStatement.executeUpdate() == 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return check;
	}
}

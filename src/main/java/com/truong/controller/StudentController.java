package com.truong.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.truong.bean.Student;
import com.truong.model.StudentDAO;

@WebServlet("/")
public class StudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String url = request.getServletPath();
		System.out.println(url);
		
		switch(url){
			case "/add": {
				this.add(request, response);
				break;
			}
			case "/": {
				getAll(request, response);
				break;
			}
			case "/store": {
				store(request, response);
				break;
			}
			case "/edit": {
				edit(request, response);
				break;
			}
			case "/update": {
				update(request, response);
				break;
			}
			case "/delete": {
				delete(request, response);
				break;
			}
			default:
				break;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public void getAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Student> list = StudentDAO.getAllStudent();
		request.setAttribute("list", list);
		RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
		dispatcher.forward(request, response);
	}
	
	public void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("formAdd.jsp");
		dispatcher.forward(request, response);
	}
	
	public void store(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Student student = new Student(
				request.getParameter("name"),
				Integer.parseInt(request.getParameter("age")),
				request.getParameter("className"));
		if(StudentDAO.store(student)) {
			response.sendRedirect("/QLSV/");
		} else {
			System.out.println("lỗi");
		}
	}
	
	public void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idCurrent = Integer.parseInt(request.getParameter("id"));
		Student student = StudentDAO.getStudent(idCurrent);
		request.setAttribute("student", student);
		RequestDispatcher dispatcher = request.getRequestDispatcher("edit.jsp");
		dispatcher.forward(request, response);
	}
	
	public void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Student student = new Student(Integer.parseInt(request.getParameter("id")),
				request.getParameter("name"),
				Integer.parseInt(request.getParameter("age")),
				request.getParameter("className"));
		if(StudentDAO.update(student)) {
			System.out.println("Thành công");
			response.sendRedirect("/QLSV/");
		} else {
			System.out.println("lỗi");
		}
	}
	
	public void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idCurrent = Integer.parseInt(request.getParameter("id"));
		if(StudentDAO.delete(idCurrent)) {
			System.out.println("Thành công");
			response.sendRedirect("/QLSV/");
		} else {
			System.out.println("lỗi");
		}
	}
	

}

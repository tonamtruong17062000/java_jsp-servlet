<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
.center {
	margin-left: auto;
	margin-right: auto;
}

td {
	padding: 10px;
}
</style>
</head>
<body>
	<div>
		<h1 style="text-align: center;">Danh sách sinh viên</h1>
		<div style="text-align: center;">
			<a href="add">Thêm mới</a>
		</div>
		<table class="center" border="1">
			<tr>
				<th>ID</th>
				<th>Họ và tên</th>
				<th>Tuổi</th>
				<th>Lớp</th>
				<th>Thao tác</th>
			</tr>
			<c:forEach items="${list}" var="student">
				<tr>
					<td>${student.id}</td>
					<td>${student.name}</td>
					<td>${student.age}</td>
					<td>${student.className}</td>
					<td>
						<a href="edit?id=${student.id}">Sửa</a>
						<a href="delete?id=${student.id}">Xóa</a>
					</td>
				</tr>
			</c:forEach>

		</table>
	</div>
</body>
</html>
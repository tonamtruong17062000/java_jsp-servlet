<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
.center {
	margin-left: auto;
	margin-right: auto;
}

td{
padding: 10px;}
</style>
</head>
<body>
	<div>
		<h1 style="text-align: center;">Thêm mới sinh viên</h1>
		<div style="text-align: center;">
			<form action="store" method="post">
			 	<table class="center" border="1">
			 		<tr>
			 			<td>Họ và tên: </td>
			 			<td>
			 				<input type="text" name="name">
			 			</td>
			 		</tr>
			 		<tr>
			 			<td>Tuổi: </td>
			 			<td>
			 				<input type="number" name="age">
			 			</td>
			 		</tr>
			 		<tr>
			 			<td>Lớp: </td>
			 			<td>
			 				<input type="text" name="className">
			 			</td>
			 		</tr>
			 		<tr>
			 			<td colspan="2">
			 				<input value="Lưu" type="submit">
			 				<a href="list" >Hủy</a>
			 			</td>
			 		</tr>
			 	</table>
			</form>
		</div>
	</div>
</body>
</html>